package restassuredTests;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class Demo5_BasicValidation_JSON {
    @Test(priority=1)
    public void testStatusCode(){
        given()
                .when()
                    .get("https://jsonplaceholder.typicode.com/posts/2")
                .then()
                    .statusCode(200);
                    //.log().all();
    }
    @Test(priority = 2)
    public void testLogging(){
        given()
                .when()
                    .get("https://jsonplaceholder.typicode.com/posts/2")
                .then()
                    .statusCode(200)
                    .log().all();
    }
    @Test(priority = 3)
    public void testSingleContent(){
        given()
                .when()
                    .get("https://jsonplaceholder.typicode.com/posts/2")
                .then()
                    .statusCode(200)
                    .body("title",equalTo("qui est esse"));
    }
    @Test(priority = 4)
    public void testMultipleContent(){
        given()
                .when()
                    .get("https://jsonplaceholder.typicode.com/posts")
                .then()
                    .statusCode(200)
                    .body("userId",hasItems(1,2,3));
    }
    @Test(priority = 5)
    public void testParametersHeaders(){
        given()
                .param("Sreyan","Biswas")
                .header("MyHeader","Indian")
                .when()
                    .get("https://jsonplaceholder.typicode.com/posts/2")
                .then()
                    .statusCode(200)
                    .body("title",equalTo("qui est esse"));
    }
}
