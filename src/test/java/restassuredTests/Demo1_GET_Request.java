package restassuredTests;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class Demo1_GET_Request {
    @Test
    public void getEmployeeDetails(){
       given()
               .when()
                   .get("https://reqres.in/api/users/2")
               .then()
                   .statusCode(200)
                   .statusLine("HTTP/1.1 200 OK")
                   .assertThat().body("data.id",equalTo(2))
                   .header("Content-Type","application/json; charset=utf-8");
    }
}
