package restassuredTests;

import org.apache.commons.lang3.RandomStringUtils;

public class REST_Utils {
    public static String getName(){
        String name = RandomStringUtils.randomAlphabetic(5);
        return name;
    }
    public static String getJob(){
        String job = RandomStringUtils.randomAlphabetic(6);
        return job;
    }
    public static String empName(){
        String nm = RandomStringUtils.randomAlphabetic(5);
        return nm;
    }
    public static String empSal(){
        String sal = RandomStringUtils.randomNumeric(4);
        return sal;
    }
    public static String empAge(){
        String age = RandomStringUtils.randomNumeric(2);
        return age;
    }
}
