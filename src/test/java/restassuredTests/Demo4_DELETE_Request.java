package restassuredTests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class Demo4_DELETE_Request {
    @Test
    public void deleteEmpRecord(){
        RestAssured.baseURI = "https://dummy.restapiexample.com/api/v1";
        RestAssured.basePath = "/delete/16";

        Response response =
        given()
                .when()
                    .delete()
                .then()
                    .statusCode(200)
                    .statusLine("HTTP/1.1 200 OK")
                    .log().all()
                .extract().response();

        String jsonString = response.asString();
        Assert.assertEquals(jsonString.contains("Successfully! Record has been deleted"),true);
    }
}
