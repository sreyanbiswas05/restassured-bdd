package restassuredTests;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import java.util.HashMap;

import static org.hamcrest.Matchers.*;

public class Demo2_POST_Request {
    public static HashMap map = new HashMap();
    String name;
    String job;
    @BeforeClass
    public void postData(){
        name = REST_Utils.getName();
        job = REST_Utils.getJob();
        map.put("name",name);
        map.put("job",job);

        RestAssured.baseURI = "https://reqres.in/api";
        RestAssured.basePath = "/users";
    }
    @Test
    public void testPost(){
        given()
                .contentType("application/json")
                .body(map)
                .when()
                    .post()
                .then()
                    .statusCode(201)
                    .body("name",equalTo(name))
                    .body("job",equalTo(job));
    }
}
