package restassuredTests;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;

import static io.restassured.RestAssured.given;

public class Demo3_PUT_Request {
    public static HashMap map = new HashMap();
    String empName = REST_Utils.empName();
    String empSalary = REST_Utils.empSal();
    String empAge = REST_Utils.empAge();
    int empId = 1;
    @BeforeClass
    public void putData(){
        map.put("name",empName);
        map.put("salary",empSalary);
        map.put("age",empAge);
        RestAssured.baseURI = "https://dummy.restapiexample.com/api/v1";
        RestAssured.basePath = "/update/"+empId;
    }
    @Test
    public void updateDetails(){
        given()
                .contentType("application/json")
                .body(map)
                .when()
                .put()
                .then()
                .statusCode(200)
                .log().all();


    }
}
