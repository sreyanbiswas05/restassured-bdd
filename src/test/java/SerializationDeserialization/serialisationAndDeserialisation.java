package SerializationDeserialization;


import java.io.*;

class Test implements Serializable {
    int i=10,j=20;
}

public class serialisationAndDeserialisation {

    public static void main(String args[]) throws IOException, ClassNotFoundException {
        Test t1 = new Test();
        FileOutputStream fout = new FileOutputStream("test.txt");
        ObjectOutputStream out = new ObjectOutputStream(fout);
        out.writeObject(t1);

        FileInputStream fin = new FileInputStream("test.txt");
        ObjectInputStream in = new ObjectInputStream(fin);
        Test t2 = (Test)in.readObject();

        System.out.println(t2.i+"  "+t2.j);
    }
}
